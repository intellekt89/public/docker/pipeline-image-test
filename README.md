# pipeline-image

## Test
```
docker run --rm -i hadolint/hadolint < Dockerfile
```

## With tag:
```
registry.gitlab.com/intellekt89/public/docker/pipeline-image:v4
```

## Latest:
```
registry.gitlab.com/intellekt89/public/docker/pipeline-image
```
